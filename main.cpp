#include "geometry.hpp"
#include "model.hpp"
#include "renderer.hpp"
#include "tgaimage.h"

#include <algorithm>
#include <iostream>
#include <cmath>
#include <cassert>
#include <limits>

const TGA::Color white = TGA::Color(255, 255, 255, 255);
const TGA::Color red = TGA::Color(255, 0, 0, 255);
const TGA::Color green = TGA::Color(0, 255, 0, 255);
const TGA::Color blue = TGA::Color(0, 0, 255, 255);

const int width = 2000;
const int height = 2000;

template <typename T>
void sort_by_y(T& v0, T& v1, T& v2)
{
        if (v0.y > v1.y) {
                std::swap(v0, v1);
        }
        if (v1.y > v2.y) {
                std::swap(v1, v2);
        }
        if (v0.y > v1.y) {
                std::swap(v0, v1);
        }
}

void
get_points_between(vec2i p0, vec2i p1, std::vector<vec2i>& s)
{
        bool tr = false;
        if (std::abs(p0.x - p1.x) < std::abs(p0.y - p1.y)) {
                std::swap(p0.x, p0.y);
                std::swap(p1.x, p1.y);
                tr = true;
        }
        if (p0.x > p1.x) {
                std::swap(p0.x, p1.x);
                std::swap(p0.y, p1.y);
        }
        const int dx = p1.x - p0.x;
        const int dy = p1.y - p0.y;
        const int derr = std::abs(dy * 2);
        int err = 0;
        int y = p0.y;
        for (int x = p0.x; x <= p1.x; ++x) {
                s.push_back(vec2i((tr ? y : x), (tr ? x : y)));
                err += derr;
                if (err > dx) {
                        y += (p0.y > p1.y ? -1 : 1);
                        err -= 2 * dx;
                }
        }
}

void fill_pixels(TGA::Image& im, const std::vector<vec2i>& s, const TGA::Color& c)
{
        typedef std::vector<vec2i> S;
        for (S::const_iterator it = s.begin(); it != s.end(); ++it) {
                im.set(it->x, it->y, c);
        }
}

void
draw_line_2d(vec2i p0, vec2i p1, TGA::Image& im, const TGA::Color& c)
{
        typedef std::vector<vec2i> S;
        S s;
        s.reserve(std::max(std::abs(p0.y - p1.y), std::abs(p0.x - p1.x)));
        get_points_between(p0, p1, s);
        fill_pixels(im, s, c);
}

void
draw_line_3d(vec3f p0, vec3f p1, TGA::Image& im, const TGA::Color& c, float* zb)
{
        vec3i pi0(p0);
        vec3i pi1(p1);
        const int max_proj = std::max(std::abs(pi0.x - pi1.x), std::abs(pi0.y - pi1.y));
        if (max_proj == 0) {
                const int minz = std::min(pi0.z, pi1.z);
                if (zb[pi0.x * width + pi0.y] > minz) {
                        zb[pi0.x * width + pi0.y] = minz;
                        im.set(pi0.x, pi0.y, c);
                }
                return;
        }
        vec3f v = p1 - p0;
        const float phi = 1 / static_cast<float>(max_proj);
        for (int i = 0; i <= max_proj; ++i) {
                vec3f n = p0 + v * (phi * i);
                vec3i ni(n);
                const int ind = ni.x * width + ni.y;
                if (zb[ind] > n.z) {
                        zb[ind] = n.z;
                        im.set(ni.x, ni.y, c);
                }
        }
}

void
draw_filled_triangle_3d(vec3f p0, vec3f p1, vec3f p2, TGA::Image& im, const TGA::Color& c, float* zb)
{
        sort_by_y(p0, p1, p2);
        draw_line_3d(p0, p1, im, c, zb);
        draw_line_3d(p1, p2, im, c, zb);
        draw_line_3d(p2, p0, im, c, zb);
        const int big_segment = round(p2.y) - round(p0.y);
        if (big_segment == 0) {
                return;
        }
        const int a_segment = round(p1.y) - round(p0.y);
        const int b_segment = round(p2.y) - round(p1.y);
        assert(a_segment != 0 || b_segment != 0);
        const float phi = 1 / static_cast<float>(big_segment);
        const float alpha = (a_segment == 0 ? 0 : 1 / static_cast<float>(a_segment));
        const float beta = (b_segment == 0 ? 0 : 1 / static_cast<float>(b_segment));
        for (int i = 0; i <= big_segment; ++i) {
                vec3f V = p0 + (p2 - p0) * phi * i;
                vec3f S;
                if (i <= a_segment) {
                        if (a_segment == 0) {
                                continue;
                        }
                        S = p0 + (p1 - p0) * alpha * i;
                }
                if (i > a_segment) {
                        if (b_segment == 0) {
                                continue;
                        }
                        S = p1 + (p2 - p1) * beta * (i - a_segment);
                }
                if (S.x > V.x) {
                        std::swap(S, V);
                }
                draw_line_3d(S, V, im, c, zb);
        }
}

std::string output_filename_option = "-tga";
std::string output_filename = "output.tga";
std::string input_filename_option = "-obj";
std::string input_filename = "./obj/african_head.obj";
std::string texture_filename_option = "-tex";
std::string texture_filename = "";

int
main(int argc, char** argv) {
        for (int i = 1; i < argc; ++i) {
                if (output_filename_option.compare(argv[i]) == 0) {
                        assert(i + 1 < argc);
                        ++i;
                        output_filename = argv[i];
                }
                if (input_filename_option.compare(argv[i]) == 0) {
                        assert(i + 1 < argc);
                        ++i;
                        input_filename = argv[i];
                }
        }
        TGA::Image image(width, height, TGA::Image::RGB);
        for (int k = 0; k < width; ++k) {
                for (int j = 0; j < height; ++j) {
                        image.set(k, j, red);
                }
        }
        static float zbuffer[width * height];
        for (int i = 0; i < width * height; ++i) {
                zbuffer[i] = std::numeric_limits<float>::max();
        }
        model m;
        m.load_from_file(input_filename);
        renderer r(1200, 1200);
        TGA::Image* im = r.render(&m, matrix::identity(4));
        //const int w = width / 2;
        //const int h = height / 2;
        //const vec3f light(0, 0, 1);
        //typedef std::vector<face>::iterator IT;
        //for (IT it = m.m_faces.begin(); it != m.m_faces.end(); ++it) {
        //        const face& f = *it;
        //        assert(f.m_vertices.size() == 3);
        //        const vec3f& wv0 = m.m_verts[f.m_vertices[0]];
        //        const vec3f& wv1 = m.m_verts[f.m_vertices[1]];
        //        const vec3f& wv2 = m.m_verts[f.m_vertices[2]];
        //        vec3f n = (wv1 - wv0) ^ (wv2 - wv0);
        //        n.normalize();
        //        float i = n * light;
        //        const vec3i scv0((wv0.x + 1) * w, (wv0.y + 1) * h, -(wv0.z * 500));
        //        const vec3i scv1((wv1.x + 1) * w, (wv1.y + 1) * h, -(wv1.z * 500));
        //        const vec3i scv2((wv2.x + 1) * w, (wv2.y + 1) * h, -(wv2.z * 500));
        //        if (i > 0) {
        //                draw_filled_triangle_3d(scv0, scv1, scv2, image,
        //                                        TGA::Color(i * 255, i * 255,  i * 255, 255), zbuffer);
        //        }
        //}
        //image.flip_vertically();
        im->write_tga_file(output_filename.c_str());
        return 0;
}

