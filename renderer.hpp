#ifndef RENDERER_HPP
#define RENDERER_HPP

#include "geometry.hpp"
#include "tgaimage.h"

class model;
class face;

class renderer
{
public:
        renderer(const int& w, const int& h);

public:
        /**
         * @param m Model to render
         * @param mt Model tranformation matrix (model -> world coords)
         */
        TGA::Image* render(const model* m, const matrix& mt);

private:
        void draw_face(const model* m, const face& f, const matrix& mt);
        void draw_triangle(std::vector<vec3f>& v);
        void draw_line(vec3f p0, vec3f p1);

private:
        int m_scene_width;
        int m_scene_height;
        float* m_zbuffer;
        matrix m_view;
        matrix m_projection;
        matrix m_viewport;
        TGA::Image* m_image;
};

#endif
