#include "model.hpp"

#include <sstream>
#include <fstream>

void model::
load_from_file(const std::string& f)
{
        std::ifstream in;
        in.open (f.c_str(), std::ifstream::in);
        if (in.fail()) {
                std::cerr << "Failed to open " << f << std::endl;
                return;
        }
        std::string line;
        while (!in.eof()) {
                std::getline(in, line);
                std::istringstream iss(line.c_str());
                char trash;
                if (!line.compare(0, 2, "v ")) {
                        iss >> trash;
                        vec3f v;
                        for (int i=0;i<3;i++) iss >> v.raw[i];
                        m_verts.push_back(v);
                } else if (!line.compare(0, 2, "f ")) {
                        face f;
                        int vi, ti, ni;
                        iss >> trash;
                        while (iss >> vi >> trash >> ti >> trash >> ni) {
                                --vi; --ni;  // in wavefront obj all indices start at 1, not zero
                                f.m_vertices.push_back(vi);
                                f.m_vertex_normals.push_back(ni);
                        }
                        m_faces.push_back(f);
                } else if (!line.compare(0, 3, "vn ")) {
                        iss >> trash;
                        vec3f v;
                        for (int i=0;i<3;i++) iss >> v.raw[i];
                        m_normals.push_back(v);
                }
        }
}
