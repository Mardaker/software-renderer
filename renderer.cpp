#include "renderer.hpp"

#include "geometry.hpp"
#include "tgaimage.h"
#include "model.hpp"

#include <algorithm>
#include <cassert>
#include <limits>

namespace {

bool
compare_by_y(vec3f v0, vec3f v1)
{
        return v0.y < v1.y;
}

matrix
viewport(int x, int y, int w, int h, int depth) {
        matrix m = matrix::identity(4);
        m[0][3] = x + w / 2.f;
        m[1][3] = y + h / 2.f;
        m[2][3] = depth / 2.f;
        m[0][0] = w / 2.f;
        m[1][1] = h / 2.f;
        m[2][2] = depth / 2.f;
        return m;
}

}

renderer::
renderer(const int& w, const int& h)
        : m_scene_width(w)
        , m_scene_height(h)
        , m_image(0)

{
        assert(m_scene_width > 0);
        assert(m_scene_height > 0);
        m_view = matrix::identity(4);
        m_projection = matrix::identity(4);
        m_viewport = viewport(m_scene_width / 8, m_scene_height / 8,
                              (m_scene_width * 3) / 4, (m_scene_height * 3) / 4, -255);
        m_zbuffer = new float[m_scene_width * m_scene_height];
        for (int i = 0; i < m_scene_width * m_scene_height; ++i) {
                m_zbuffer[i] = std::numeric_limits<float>::max();
        }
}

TGA::Image* renderer::
render(const model* m, const matrix& mt)
{
        assert(m != 0);
        assert(m_image == 0);
        m_image = new TGA::Image(m_scene_width, m_scene_height, TGA::Image::RGB);
        std::vector<face>::const_iterator it = m->m_faces.begin();
        for (; it != m->m_faces.end(); ++it) {
                draw_face(m, *it, mt);
        }
        TGA::Image* im = m_image;
        m_image = 0;
        return im;
}

void renderer::
draw_face(const model* m, const face& f, const matrix& mt)
{
        assert(f.m_vertices.size() == 3);
        assert(f.m_vertex_normals.size() == 3);
        std::vector<vec3f> screen_coords;
        for (int i = 0; i < 3; ++i) {
                const vec3f& mv = m->m_verts[f.m_vertices[i]];
                const vec3f& sv = matrix::to_vector(m_viewport * m_projection * m_view * mt * (matrix::from_vector(mv)));
                //vec3f sv((mv.x + 1) * m_scene_width / 2,
                //               (mv.y + 1) * m_scene_height / 2,
                //               -mv.z * 255);
                screen_coords.push_back(sv);
        }
        draw_triangle(screen_coords);
}

void renderer::
draw_triangle(std::vector<vec3f>& v)
{
        std::sort(v.begin(), v.end(), compare_by_y);
        const int big_segment = round(v[2].y) - round(v[0].y);
        if (big_segment == 0) {
                return;
        }
        draw_line(v[0], v[1]);
        draw_line(v[1], v[2]);
        draw_line(v[2], v[0]);
        const int a_segment = round(v[1].y) - round(v[0].y);
        const int b_segment = round(v[2].y) - round(v[1].y);
        assert(a_segment != 0 || b_segment != 0);
        const float phi = 1 / static_cast<float>(big_segment);
        const float alpha = (a_segment == 0 ? 0 : 1 / static_cast<float>(a_segment));
        const float beta = (b_segment == 0 ? 0 : 1 / static_cast<float>(b_segment));
        for (int i = 0; i <= big_segment; ++i) {
                vec3f V = v[0] + (v[2] - v[0]) * phi * i;
                vec3f S;
                if (i <= a_segment) {
                        if (a_segment == 0) {
                                continue;
                        }
                        S = v[0] + (v[1] - v[0]) * alpha * i;
                }
                if (i > a_segment) {
                        if (b_segment == 0) {
                                continue;
                        }
                        S = v[1] + (v[2] - v[1]) * beta * (i - a_segment);
                }
                if (S.x > V.x) {
                        std::swap(S, V);
                }
                draw_line(S, V);
        }
}

void renderer::
draw_line(vec3f p0, vec3f p1)
{
        vec3i pi0(p0);
        vec3i pi1(p1);
        const int max_proj = std::max(std::abs(pi0.x - pi1.x), std::abs(pi0.y - pi1.y));
        if (max_proj == 0) {
                const int minz = std::min(pi0.z, pi1.z);
                const int ind = pi0.x * m_scene_width + pi0.y;
                if (minz < m_zbuffer[ind]) {
                        m_zbuffer[ind] = minz;
                        m_image->set(pi0.x, pi0.y, TGA::Color(100, 100, 100, 255));
                }
                return;
        }
        vec3f v = p1 - p0;
        const float phi = 1 / static_cast<float>(max_proj);
        for (int i = 0; i <= max_proj; ++i) {
                vec3f n = p0 + v * (phi * i);
                vec3i ni(n);
                const int ind = ni.x * m_scene_width + ni.y;
                if (n.z < m_zbuffer[ind]) {
                        m_zbuffer[ind] = n.z;
                        m_image->set(ni.x, ni.y, TGA::Color(100, 100, 100, 255));
                }
        }
}
