#ifndef GEOMETRY_HPP
#define GEOMETRY_HPP

#include <cmath>
#include <iostream>
#include <vector>

template <typename T>
struct vec2
{
        union {
                struct {T x, y;};
                struct {T u, v;};
                T raw[2];
        };
        vec2() : x(0), y(0) {}
        vec2(T f, T s) : x(f), y(s) {}
        inline vec2<T> operator+ (const vec2<T>& o) const
        {
                return vec2<T>(x + o.x, y + o.y);
        }

        inline vec2<T> operator- (const vec2<T>& o) const
        {
                return vec2<T>(x - o.x, y - o.y);
        }

        inline vec2<T> operator* (const float& f) const
        {
                return vec2<T>(x * f , y * f);
        }
        template <class> friend std::ostream& operator<< (std::ostream& s, vec2<T>& v);
};

typedef vec2<float> vec2f;
typedef vec2<int> vec2i;

template <class T> std::ostream& operator<<(std::ostream& s, vec2<T>& v) {
        s << "(" << v.x << ", " << v.y << ")" << std::endl;
        return s;
}

template <typename T>
struct vec3
{
        union {
                struct {T x, y, z;};
                struct {T u, v, n;};
                T raw[3];
        };
        vec3<T>() : x(0), y(0), z(0) {}
        vec3<T>(T f, T s, T t) : x(f), y(s), z(t) {}

        template <class U>
        vec3<T>(const vec3<U>& u);

        inline vec3<T> operator^ (const vec3<T> &o) const
        {
                return vec3<T>(y * o.z - z * o.y, z * o.x - x * o.z, x * o.y - y * o.x);
        }

        inline vec3<T> operator+ (const vec3<T> &o) const
        {
                return vec3<T>(x + o.x, y + o.y, z + o.z);
        }

        inline vec3<T> operator- (const vec3<T> &o) const
        {
                return vec3<T>(x - o.x, y - o.y, z - o.z);
        }

        inline T operator* (const vec3<T> o) const
        {
                return x * o.x + y * o.y + z * o.z;
        }

        inline vec3<T> operator* (const float& f) const
        {
                return vec3<T>(x * f, y * f, z * f);
        }

        float norm() const
        {
                return std::sqrt((x * x) + (y * y) + (z * z));
        }

        vec3<T>& normalize(T l = 1)
        {
                *this = (*this) * (l / norm());
                return *this;
        }

        template <class> friend std::ostream& operator<< (std::ostream& s, vec3<T>& v);
};

typedef vec3<float> vec3f;
typedef vec3<int> vec3i;

template <class T> std::ostream& operator<<(std::ostream& s, vec3<T>& v) {
        s << "(" << v.x << ", " << v.y << ", " << v.z << ")" << std::endl;
        return s;
}

namespace std {

template <typename T>
void swap(vec2<T>& v0, vec2<T>& v1)
{
        std::swap(v0.x, v1.x);
        std::swap(v0.y, v1.y);
}

template <typename T>
void swap(vec3<T>& v0, vec3<T>& v1)
{
        std::swap(v0.x, v1.x);
        std::swap(v0.y, v1.y);
        std::swap(v0.z, v1.z);
}

} // namespace std

class matrix {
public:
        matrix(int r = 4, int c = 4);

public:
        inline int row_count() const;
        inline int column_count() const;

public:
        static matrix identity(int dimensions);
        static vec3f to_vector(const matrix&);
        static matrix from_vector(const vec3f&);

public:
        matrix transpose();
        matrix inverse();

public:
        std::vector<float>& operator[](const int i);
        const std::vector<float>& operator[](const int i) const;
        matrix operator*(const matrix& a);

        friend std::ostream& operator<<(std::ostream& s, matrix& m);

private:
        std::vector<std::vector<float> > m;
        int m_row_count, m_column_count;

};

#endif // GEOMETRY_HPP
