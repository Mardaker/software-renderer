#include "geometry.hpp"

#include <cassert>
#include <cmath>

template <> template <> vec3<int>::vec3(const vec3<float> &v) : x(round(v.x)), y(round(v.y)), z(round(v.z))
{}

template <> template <> vec3<float>::vec3(const vec3<int> &v) : x(v.x), y(v.y), z(v.z)
{}

matrix::
matrix(int r, int c)
        : m(std::vector<std::vector<float> >(r, std::vector<float>(c, 0.f)))
        , m_row_count(r)
        , m_column_count(c)
{}

int matrix::
row_count() const
{
        return m_row_count;
}

int matrix::
column_count() const
{
        return m_column_count;
}

matrix matrix::
identity(int d) {
        matrix E(d, d);
        for (int i = 0; i < d; ++i) {
                for (int j = 0; j < d; ++j) {
                        E[i][j] = (i==j ? 1. : 0.);
                }
        }
        return E;
}

vec3f matrix::
to_vector(const matrix& m)
{
        assert(m.column_count() == 1);
        return vec3f(m[0][0], m[1][0], m[2][0]);
}

matrix matrix::
from_vector(const vec3f& v)
{
        matrix m(4, 1);
        m[0][0] = v.x;
        m[1][0] = v.y;
        m[2][0] = v.z;
        m[3][0] = 1;
        return m;
}

std::vector<float>& matrix::
operator[](const int i)
{
        assert(i >= 0 && i < m_row_count);
        return m[i];
}

const std::vector<float>& matrix::
operator[](const int i) const
{
        assert(i >= 0 && i < m_row_count);
        return m[i];
}

matrix matrix::
operator*(const matrix& a)
{
        assert(m_column_count == a.m_row_count);
        matrix r(m_row_count, a.m_column_count);
        for (int i = 0; i < m_row_count; ++i) {
                for (int j = 0; j < a.m_column_count; ++j) {
                        r.m[i][j] = 0.;
                        for (int k = 0; k < m_column_count; ++k) {
                                r.m[i][j] += m[i][k]*a.m[k][j];
                        }
                }
        }
        return r;
}

matrix matrix::
transpose()
{
        matrix r(m_column_count, m_row_count);
        for(int i = 0; i < m_row_count; ++i)
                for(int j = 0; j < m_column_count; ++j)
                        r[j][i] = m[i][j];
        return r;
}

matrix matrix::
inverse()
{
        assert(m_row_count==m_column_count);
        // augmenting the square matrix with the identity matrix
        // of the same dimensions a => [ai]
        matrix r(m_row_count, m_column_count*2);
        for(int i = 0; i < m_row_count; ++i)
                for(int j = 0; j < m_column_count; ++j)
                        r[i][j] = m[i][j];
        for(int i = 0; i < m_row_count; ++i)
                r[i][i+m_column_count] = 1;
        // first pass
        for (int i = 0; i < m_row_count - 1; ++i) {
                // normalize the first row
                for(int j = r.m_column_count - 1; j >= 0; --j)
                        r[i][j] /= r[i][i];
                for (int k = i + 1; k < m_row_count; ++k) {
                        float c = r[k][i];
                        for (int j = 0; j < r.m_column_count; ++j) {
                                r[k][j] -= r[i][j] * c;
                        }
                }
        }
        // normalize the last row
        for(int j = r.m_column_count - 1; j >= m_row_count - 1; --j)
                r[m_row_count - 1][j] /= r[m_row_count - 1][m_row_count - 1];
        // second pass
        for (int i = m_row_count - 1; i > 0; --i) {
                for (int k = i - 1; k >= 0; --k) {
                        float c = r[k][i];
                        for (int j = 0; j < r.m_column_count; ++j) {
                                r[k][j] -= r[i][j] * c;
                        }
                }
        }
        // cut the identity matrix back
        matrix t(m_row_count, m_column_count);
        for(int i = 0; i < m_row_count; ++i)
                for(int j = 0; j < m_column_count; ++j)
                        t[i][j] = r[i][j + m_column_count];
        return t;
}

std::ostream&operator<<(std::ostream& s, matrix& m)
{
        for (int i = 0; i < m.row_count(); ++i) {
                for (int j = 0; j < m.column_count(); j++) {
                        s << m[i][j];
                        if (j < m.column_count() - 1) {
                                s << "\t";
                        }
                }
                s << std::endl;
        }
        return s;
}
