#ifndef MODEL_HPP
#define MODEL_HPP

#include "geometry.hpp"

#include <string>
#include <vector>

struct face {
        std::vector<int> m_vertices;
        std::vector<int> m_vertex_normals;
};

class model {

public:
        void load_from_file(const std::string& f);

public:
        std::vector<vec3f> m_verts;
        std::vector<vec3f> m_normals;
        std::vector<face> m_faces;
};

#endif
